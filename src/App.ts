import { Application } from 'pixi.js';
import Floor from './Floor';
import Elevator from './Elevator';
import Move from './utils/Move';

export default class App extends Application {
  floorCount: number;
  floors: Floor[];
  elevator: Elevator;
  currentFloor: number;
  floorQueue: number[];
  move: Move | null;

  constructor(...props: ConstructorParameters<typeof Application>) {
    super(...props);

    this.currentFloor = 0;
    this.floorQueue = [];
    this.move = null;

    this.floorCount = 4;
    this.floors = this.generateFloors();

    this.elevator = new Elevator(this.floorHeight)

    // Mounting elements
    this.floors.forEach(floor => {
      floor.y = this.floorHeight * floor.floorNumber;
      this.stage.addChild(floor);
    });
    this.elevator.x = this.view.width * 2 / 3;
    this.elevator.y = this.currentFloor * this.floorHeight;
    this.stage.addChild(this.elevator);


    this.ticker.add((delta: number) => {
      if (this.move) {
        this.move.animate(delta);
      } else if (this.floorQueue.length) {
        const nexFloor = this.floorQueue.shift();
        if (nexFloor !== undefined) this.move = this.moveToFloor(nexFloor);
      };
    });
  };

  get floorHeight(): number {
    return this.view.height / this.floorCount
  }

  generateFloors() {
    const floors: Floor[] = [];
    for (let i = 0; i < this.floorCount; i++) {
      const floor = new Floor(this.view.width, this.floorHeight, i);
      floor.on('requestFloor', floorNumber => this.queueFloor(floorNumber));

      floors.push(floor);
    };

    return floors;
  };

  queueFloor(floor: number) {
    this.floorQueue.push(floor)
  };

  moveToFloor(floor: number) {
    const floorRange = floor - this.currentFloor;
    const start = this.elevator.y;
    const distance = floorRange * this.floorHeight;
    const animDuration = Math.abs(floorRange * 150);

    return new Move(position => {
      this.elevator.y = start + distance * position;
    }, animDuration, () => {
      this.currentFloor = floor;
      this.move = null;
    }).next(this.elevator.openDoors());
  };
};
