import { Graphics } from "pixi.js";

export default class Door extends Graphics {
    elementWidth: number;
    elementHeight: number;

    constructor(width: number, height: number) {
        super();
        this.elementHeight = height;
        this.elementWidth = width;

        this.draw();
    };

    draw() {
        this.beginFill(0x203544, 1);
        this.drawRect(0, 0, this.elementWidth, this.elementHeight)
        this.endFill();
    };
};