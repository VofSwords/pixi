import { Container, Graphics } from 'pixi.js';
import Door from './Door';
import Move from '../utils/Move';

export default class Elevator extends Container {
    elevatorHeight: number;
    leftDoor: Door;
    rightDoor: Door;
    cabine: Graphics;

    constructor(height: number) {
        super();

        this.elevatorHeight = height;

        const doorHeight = this.elevatorHeight - 40;
        const doorWidth = this.elevatorWidth / 2 - 6;

        this.leftDoor = new Door(doorWidth, doorHeight);
        this.rightDoor = new Door(doorWidth, doorHeight);
        this.cabine = this.draw();


        this.leftDoor.x = 3;
        this.rightDoor.x = this.elevatorWidth / 2 + 3;
        this.leftDoor.y = this.rightDoor.y = 20;
        this.addChild(this.cabine);
        this.addChild(this.leftDoor);
        this.addChild(this.rightDoor)
    }

    get elevatorWidth(): number {
        return this.elevatorHeight * 2 / 3;
    };

    draw() {
        const mainColor = 0x5386aa;

        const cabine = new Graphics();

        cabine.beginFill(mainColor, 1);
        cabine.drawRect(0, 0,this.elevatorWidth , this.elevatorHeight)
        cabine.endFill();

        return cabine;
    };

    openDoors(): Move {
        const leftDoorStart = this.leftDoor.x;
        const rightDoorStart = this.rightDoor.x;

        return new Move(position => {
            const doorDelta = Math.sin(position * Math.PI) * 30;
            this.leftDoor.x = leftDoorStart - doorDelta;
            this.rightDoor.x = rightDoorStart + doorDelta;

        }, 100);
    };
};