import { Container, Graphics } from 'pixi.js';

export default class Floor extends Container{
    floorWidth: number;
    floorHeight: number;
    floorNumber: number;

    constructor(width: number, height: number, floorNumber: number) {
        super();
        this.floorWidth = width;
        this.floorHeight = height;
        this.interactive = true;
        this.floorNumber = floorNumber;
        this.on('pointerdown', () => this.emit('requestFloor', this.floorNumber));

        this.draw();
    };

    draw() {
        const blue = 0x709FE9;
        const red = 0xD82257;

        const rect = new Graphics();

        rect.beginFill(blue, 1);
        rect.drawRect(0, 0, this.floorWidth, this.floorHeight);
        rect.endFill();
        
        rect.lineStyle(5, red, 1);
        rect.drawRect(0, 0, this.floorWidth, this.floorHeight);
        this.addChild(rect);
    };
};