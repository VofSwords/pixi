import './style.css';

import App from './App';

const appContainer = document.querySelector<HTMLDivElement>('#app')!

const app = new App({ width: 640, height: 640, backgroundColor: 0x86D0F2 });

appContainer.appendChild(app.view);