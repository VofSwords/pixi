export default class Move {
    duration: number;
    timeSpent: number;
    finished: boolean;
    queue: Move[];
    cb: AnimationFunction;
    currentMove: number;
    onEnd: () => void;

    constructor(cb: AnimationFunction, duration: number, onEnd: () => void = () => {}) {
        this.duration = duration;
        this.timeSpent = 0;
        this.finished = false;
        this.queue = [this];
        this.cb = cb;
        this.currentMove = 0;
        this.onEnd = onEnd;
    };

    get position() {
        return Math.min(this.timeSpent / this.duration, 1);
    };

    animate(delta: number) {
        const move = this.queue[this.currentMove];

        if (!move) return this.onEnd();

        if (!move.finished) {
            move.update(delta);
        } else this.currentMove++

    };

    update(delta: number) {
        this.timeSpent += delta;
        this.cb(this.position);
        this.finished = this.timeSpent > this.duration;
    };

    
    next(move: Move) {
        this.queue.push(move);
        return this;
    };
};

export type AnimationFunction = (position: number) => void;