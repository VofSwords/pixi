import { defineConfig } from 'vite';

// https://vitejs.dev/config/
export default defineConfig({
  base: '/pixi/',
  css: {
    modules: {
      localsConvention: "camelCase"
    }
  }
});
